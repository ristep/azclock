# German translation file for Desktop Clock.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# Onno Giesmann <nutzer3105@gmail.com>, 2022.
#
msgid ""
msgstr ""
"Project-Id-Version: \n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2022-06-18 22:56+0200\n"
"PO-Revision-Date: 2022-06-18 23:31+0200\n"
"Last-Translator: Onno Giesmann <nutzer3105@gmail.com>\n"
"Language-Team: \n"
"Language: de\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Poedit 3.0.1\n"

#: extension.js:242
msgid "Unlock"
msgstr "Entsperren"

#: extension.js:242
msgid "Lock"
msgstr "Sperren"

#: extension.js:244
msgid "Desktop Clock Settings"
msgstr "Desktop Clock-Einstellungen"

#: prefs.js:32
msgid "Settings"
msgstr "Einstellungen"

#: prefs.js:40
msgid "General Options"
msgstr "Allgemeine Optionen"

#: prefs.js:45
msgid "Time - Date"
msgstr "Zeit - Datum"

#: prefs.js:46
msgid "Date - Time"
msgstr "Datum - Zeit"

#: prefs.js:48
msgid "Label Order"
msgstr "Feldsortierung"

#: prefs.js:61
msgid "Display In-Line"
msgstr "In einer Reihe anzeigen"

#: prefs.js:80
msgid "Date Format"
msgstr "Datumsformat"

#: prefs.js:85
msgid "Format Guide"
msgstr "Formatleitfaden"

#: prefs.js:95
msgid "Enable Border"
msgstr "Rahmen einschalten"

#: prefs.js:107
msgid "Border Width"
msgstr "Rahmenbreite"

#: prefs.js:109
msgid "Border Radius"
msgstr "Eckenabrundung"

#: prefs.js:111
msgid "Border Color"
msgstr "Rahmenfarbe"

#: prefs.js:116
msgid "Enable Background"
msgstr "Hintergrund einschalten"

#: prefs.js:129
msgid "Background Color"
msgstr "Hintergrundfarbe"

#: prefs.js:133
msgid "Text Style"
msgstr "Textdarstellung"

#: prefs.js:138
msgid "Text Color"
msgstr "Textfarbe"

#: prefs.js:140
msgid "Time Font Size"
msgstr "Schriftgröße Uhrzeit"

#: prefs.js:142
msgid "Date Font Size"
msgstr "Schriftgröße Datum"

#: prefs.js:147
msgid "Shadow Color"
msgstr "Schattenfarbe"

#: prefs.js:149
msgid "Shadow X Offset"
msgstr "Schattenversatz (X)"

#: prefs.js:151
msgid "Shadow Y Offset"
msgstr "Schattenversatz (Y)"

#: prefs.js:153
msgid "Shadow Spread"
msgstr "Schattenausbreitung"

#: prefs.js:157
msgid "Reset Desktop Clock Settings"
msgstr "Desktop Clock-Einstellungen zurücksetzen"

#: prefs.js:164
msgid "Reset all Settings"
msgstr "Alle Einstellungen zurücksetzen"

#: prefs.js:170
msgid "Reset all settings?"
msgstr "Alle Einstellungen zurücksetzen?"

#: prefs.js:171
msgid "All Desktop Clock settings will be reset to the default value."
msgstr "Alle Desktop Clock-Einstellungen werden auf die Vorgabewerte zurückgesetzt."

#: prefs.js:191
msgid "Reset Clock Position"
msgstr "Position der Uhr zurücksetzen"

#: prefs.js:197
msgid "Reset Clock Position?"
msgstr "Position der Uhr zurücksetzen?"

#: prefs.js:198
msgid "Please confirm reset of clock position."
msgstr "Bitte bestätigen Sie das Zurücksetzen der Uhr-Position."

#: prefs.js:315
msgid "About"
msgstr "Info"

#: prefs.js:334
msgid "Desktop Clock"
msgstr "Desktop Clock"

#: prefs.js:340
msgid "Add a clock to the desktop!"
msgstr "Fügen Sie Ihrem Schreibtisch eine Uhr hinzu!"

#: prefs.js:354
msgid "Desktop Clock Version"
msgstr "Desktop Clock-Version"

#: prefs.js:367
msgid "Git Commit"
msgstr "Git-Commit"

#: prefs.js:380
msgid "GNOME Version"
msgstr "GNOME-Version"

#: prefs.js:388
msgid "OS"
msgstr "Betriebssystem"

#: prefs.js:411
msgid "Session Type"
msgstr "Sitzungsart"
